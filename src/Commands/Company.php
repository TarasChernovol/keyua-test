<?php

namespace App\Commands;

use App\Services\Employee;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class Company
 * @package App\Commands
 */
class Company extends Command
{
    /**
     * @var \App\Services\Employee
     */
    private $employee;

    /**
     * Company constructor.
     */
    public function __construct()
    {
        $this->employee = new Employee();

        parent::__construct();
    }

    /**
     *
     */
    protected function configure()
    {
        $this->setName('company:employee')->addArgument('position', InputArgument::REQUIRED);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->employee->findEmployee($input->getArgument('position'));

        $output->writeln($result);

        return Command::SUCCESS;
    }
}