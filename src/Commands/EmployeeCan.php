<?php

namespace App\Commands;

use App\Services\Employee;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class EmployeeCan
 * @package App\Commands
 */
class EmployeeCan extends Command
{
    /**
     * @var \App\Services\Employee
     */
    private $employee;

    /**
     * EmployeeCan constructor.
     */
    public function __construct()
    {
        $this->employee = new Employee();

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('employee:can')
            ->addArgument('position', InputArgument::REQUIRED)
            ->addArgument('ability', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $result = $this->employee->canEmployee($input->getArgument('position'), $input->getArgument('ability'));

        if ($result) {
            $output->writeln('true');
        } else {
            $output->writeln('false');
        }

        return EmployeeCan::SUCCESS;
    }
}