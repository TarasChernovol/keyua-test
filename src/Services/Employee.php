<?php

namespace App\Services;

/**
 * Class Employee
 */
class Employee
{
    /**
     * @var array
     */
    private $employeeMap = [
        'programmer' => ['writeCode', 'testCode', 'communicationWithManager'],
        'designer' => ['draw', 'communicationWithManager'],
        'tester' => ['testCode', 'communicationWithManager'],
        'manager' => ['communicationWithManager'],
    ];

    /**
     * @param $position
     *
     * @return string
     */
    public function findEmployee($position)
    {
        if (!empty($this->employeeMap) && array_key_exists($position, $this->employeeMap)) {
            return implode(', ', $this->employeeMap[$position]);
        }

        return 'Employee not found.';
    }

    /**
     * @param $position
     * @param $ability
     *
     * @return bool
     */
    public function canEmployee($position, $ability)
    {
        if (!empty($this->employeeMap) && array_key_exists($position, $this->employeeMap)) {
            if (in_array($ability, $this->employeeMap[$position])) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }
}